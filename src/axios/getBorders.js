import axios from 'axios';

export let keys = {
  key: '98868453c73052e17ab9df0a4edf4694',
  token: '36eddc198b3d27b2525a8a7fba48296abe009203abec8776ba305824a81f4db5',
  idTable: '',
};

function urlsConstructor(part) {
  return (
    `https://api.trello.com/1/` +
    (part === 'main' ? `members/me/boards` : `boards/${keys.idTable}/` + part) +
    `?key=${keys.key}&token=${keys.token}`
  );
}

export function getBoards(event, method) {
  return axios[method](urlsConstructor(event));
}

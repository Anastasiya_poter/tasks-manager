import { testActionTypes } from '../../constants/testConstants';

export default function testReducer(state = 'xyu', action) {
  switch (action.type) {
    case testActionTypes.TEST:
      return new Date();
    default:
      return state;
  }
}

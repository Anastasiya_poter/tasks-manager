import { testActionTypes } from '../../constants/testConstants';
import { dispatch } from '../createStore';

const test = () => {
  dispatch({ type: testActionTypes.TEST });
};

export default { test };

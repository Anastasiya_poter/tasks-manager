import React from 'react';
import './style.scss';

export default function Board(props) {
  console.log(props);

  return (
    <div className="board">
      <div className="board__name">{props.name}</div>
      <div className="board__count">count = {(props.list || []).length}</div>
      <div className="board__list">
        {props.list
          .map((item, index) => (
            <div key={`b${index}`} className="board__list-item">
              {item.name}
            </div>
        ))}
      </div>
    </div>
  );
}

import React from 'react';

import Board from '../board';
import { connect } from 'react-redux';
import testActions from '../../store/actions/testActions';

import { getBoards } from '../../axios/getBorders';
import { keys } from '../../axios/getBorders';

class Content extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      boards: {},
      main: {},
    };
  }

  componentDidMount() {
    let boards = {};

    getBoards('main', 'get')
      .then(res => {
        this.setState({
          main: res.data[0],
        });

        keys.idTable = this.state.main.id;
      })
      .then(() => {
        testActions.test();
      })
      .then(() => {
        getBoards('lists', 'get').then(res => {
          keys.idLists = res.data.map((item, index) => {

            boards[item.id] = {};
            boards[item.id].name = item.name;

            return item.id;
          });
        });
      })
      .then(() => {
        getBoards('cards', 'get').then(res => {
          res.data.forEach((item, index) => {

            let list = item.idList;
            let value = boards[list];
            (boards[list] || {}).value = (boards[list] || {}).value
              ? (boards[list] || {}).value.concat(item)
              : [item];
          });
          this.setState({
            boards: boards,
          });
        });
      });
  }

  render() {
    // console.log(this.props.pizda, this.props);
    return (
      <div className="content">
        <div className="project-name">{this.state.main.name}</div>
        <div className="boards">

          {
            Object.keys(this.state.boards).map((board, index) => (
            <Board
              key={`c${index}`}
              name={this.state.boards[board].name}
              list={this.state.boards[board].value}
            />))
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({ pizda: state.test });

export default connect(mapStateToProps)(Content);

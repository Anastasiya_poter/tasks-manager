import React from 'react';
import './App.scss';

import Content from "./components/content";

function App() {
    return (
        <div className='App'>
            <div className='App__header'>header</div>
            <div className='wrapper'>
                <div className='sidebar'>sidebar</div>
                <Content />
            </div>
            <div className='App__footer'>footer</div>
            <div className='DaD'>DaD</div>
        </div>
    )
}

export default App;
